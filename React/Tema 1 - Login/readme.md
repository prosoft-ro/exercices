# Tema 1 - Pagina de autentificare

### Realizati o pagina de login care sa respecte urmatoarele cerinte:

1. Aspect
	1. Un input de tip text in care se va introduce numele de utilizator.  
	2. Un input de tip password in care se va introduce parola (valoarea introdusa trebuie sa fie mascata).  
	3. Un buton de login.  
	4. Formularul sa fie centrat orizontal si vertical indiferent de dimensiunea ecranului.
	5. Un titlu si un subtitlu al aplicatiei (pentru titlu se poate folosi denumirea "Inventar" iar pentru subtitlu "Conectare").
	6. Un label rosu ce va afisa mesajul de eroare (daca exista).

2. Functionalitate
	1. Se va crea o variabila de tip array ce va contine 3 conturi valide:
		1. Username: Admin, Password: 1234
		2. Username: Operator, Password: 5678
		3. Username: Audit, Password: 91011
	2. La apasarea butonului de autentificare creat in pasul 1.3 se va apela o functie denumita "login".
	3. Functia "login" va verifica daca:
		1. Campul cu numele de utilizator creat in pasul 1.1 a fost completat. In caz contrar se va afisa mesajul "**Va rugam completati numele de utilizator**" in label-ul creat la pasul 1.6.
		2. Campul cu parola creat in pasul 1.2 a fost completat. In caz contrar se va afisa mesajul "**Va rugam completati parola**" in label-ul creat la pasul 1.6.
		3. Numele de utilizator exista in lista de conturi creata in pasul 2.1. In caz contrar se va afisa mesajul "**Contul** *[username]* **nu exista**" in label-ul creat la pasul 1.6, unde [username] reprezinta valoare introdusa in campul creat in pasul 1.1. Verificarea va fi case insensitive.
		4. Parola introdusa in campul creat in pasul 1.2 corespunde cu parola asociata numelui de utilizator. In caz contrar se va afisa mesajul "**Parola este incorecta**" in label-ul creat la pasul 1.6. Verificarea va fi case sensitive.
		5. Daca informatiile de login sunt corecte, se va afisa o alerta cu mesajul "**Autentificare reusita**" (de exemplu alert("Autentificare reusita");).