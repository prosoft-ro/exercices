# **Tema 1**  

## **I. Fisiere si liste**

### 1. Sa se genereze o lista de 2048 de string-uri formate din 8 cifre aleatorii. Lista va fi de tipul List<string> din namespace-ul System.Collections.Generic.  

Exemplu:   
32667824, 12683209, 32638712, 00251634, 21552165...  

### 2. Sa se salveze string-urile generate in lista de la punctul 1 intr-un fisier denumit **RandomNumbers.txt**. Fiecare string va fi scris pe un rand nou.  

Exemplu:  
32667824  
12683209  
32638712  
00251634  
21552165  
...  

### 3. Sa se citeasca fisierul **RandomNumbers.txt** si sa se incarce intr-o lista noua primele 1024 de randuri din acesta. Variabila listei va avea denumirea **loadedNumbers**  .

### 4. Sa se afiseze in consola, pe cate un rand, doar string-urile incarcate in lista loadedNumbers care contin secventa "52" sau incep cu cifra "0" sau se termina cu cifrele "01".  

Exemplu:  
00251634  
21552165  
...

### 5. Sa se afiseze in consola, separate prin virgula, toate string-urile din lista loadedNumbers care convertite in numere intregi sunt diviziblie cu 2 si cu 3.  

Exemplu:  
00251634, ...  

### 6. Sa se inverseze lista loadedNumbers.  

### 7. Sa se stearga din lista loadedNumbers elementul de pe pozitia 128.  

### 8. Sa se stearga din lista loadedNumbers elementele cuprise intre pozitia 1000 (inclusiv) si ultimul element al listei (inclusiv).  

### 9. Sa se insereze in lista loadedNumbers string-ul "11111111" pe pozitia 64.  

### 10. Sa se stearga din lista loadedNumbers elementele care se termina cu caracterul cu cod ASCII 56.  

### 11. Sa se creeze un folder numit "RandomFolder" si sa se mute fisierul "RandomNumnbers.txt" in interiorul acestuia.  

### 12. Sa se stearga folderul "RandomFolder".  

### 13. Sa se lanseze din cod un proces de Notepad sau de Word (sau un alt program la alegere).  

### 14. Daca se introduce in consolă cuvantul "open" urmat de un spatiu si numele/calea unui executabil, se va rula executabilul.  

Exemplu:  
open notepad.exe  

## **II. Clase**

