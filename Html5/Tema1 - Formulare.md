# Tema 1 - Formulare HTML5

### Realizati un formular HTML5 de abonare care sa contina:  
1. Un input de tip text cu label-ul "Nume".  
2. Un input de tip text cu label-ul "Prenume".  
3. Un input de tip email cu label-ul "Email".  
4. O lista de selectie cu label-ul "Tara" care sa contina o valoare nula si valorile "Romania", "Republica Moldova".    
5. Un grup de butoane radio cu valorile "Abonament anual", "Abonament lunar" si "Abonament saptamanal".  
6. Un camp larg de text de dimensiune fixa, cu label-ul "Observatii".  
7. Un checkbox cu label-ul "Am citit si aprob termenii si conditiile".  
8. Un buton albastru cu textul "Abonare" care la apasare afiseaza intr-o casuta de dialog:  
	* Daca checkbox-ul "Am citit si aprob termenii si conditiile" nu este bifat, afiseaza mesajul "Va rugam aprobati termenii si conditiile".  
	* Daca numele nu este completat, afiseaza mesajul "Va rugam completati numele".  
	* Daca prenumele nu este completat, afiseaza mesajul "Va rugam completati prenumele".  
	* Daca email-ul nu este completat, afiseaza mesajul "Va rugam completati email-ul".  
	* Afltfel se afiseaza mesajul "Succes!".  