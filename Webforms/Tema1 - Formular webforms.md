# Tema 1 - Formular webforms

### 1. Sa se creeze un nou proiect gol de Webforms (in VS2017 se gaseste sub Visual C# -> Web -> Previous Versions -> ASP.NET Empty Web Site).  
Nota: in loc de "ASP.NET Empty Web Site" poti alege "ASP.NET Web Site" pentru a vedea un demo de site creat de Microsoft.  

### 2. Se se creeze o pagina aspx cu denumirea "Formular.aspx".

### 3. Sa se adauge in pagina un control de tip Textbox, runat server cu ID-ul "CodTextbox".  

### 4. Sa se adauge in pagina un control de tip Label, runat server cu ID-ul "ResultLabel".  

### 5. Sa se adauge in pagina un control de tip Button, runat server, cu ID-ul "SubmitButton", textul "Submit", care la apasare sa apeleze o functie numita "Submit_Click" care va fi definita in fisierul Formular.aspx.cs.  

### 6. In metoda Submit_Click sa se citeasca textul din campul "CodTextbox" si sa se scrie in campul "ResultLabel".  

### 7. In metoda Submit_Click, sa se deschida o conexiune catre baza de date "Clinica" si sa se insereze in tabelul "Coduri", coloana "Cod" textul din campul "CodTextbox".  

### 8. Sa se transforme textbox-ul "CodTextbox" din textbox simplu in textbox multiline.  

### 9. Sa se adauge in pagina un control de tip Button, runat server, cu ID-ul "SubmitProcedureButton", textul "Submit procedure" care la apasare sa apeleze o functie numita "SubmitProcedure_Click" care va fi definita in fisierul Formular.aspx.cs.  

### 10. In metoda Submit_Click, sa se deschida o conexiune catre baza de date "Clinica" si sa se apeleze procedura stocata (stored procedure) "dbo.InserareCod", unde parametrul @cod va primi textul campului CodTextbox.  
Nota: Procedura stocata se va gasi in baza de date, sub nodul "Programability -> Stored procedures". Daca procedura este executata corect, in tabelul Coduri va aparea valoarea pasata ca parametru procedurii (la fel ca la punctul 7).  